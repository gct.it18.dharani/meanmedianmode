package com.array;

import java.util.Arrays;
import java.util.Scanner;

public class MeanMedianMode {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfElements = scanner.nextInt();
        int[] arrayElememts = new int[numberOfElements];
        for (int i = 0; i < numberOfElements; i++) {
            arrayElememts[i] = scanner.nextInt();
        }
        System.out.println(" Mean " + CalculateMean(arrayElememts));
        System.out.println(" Median " + CalculateMedian(arrayElememts));
        System.out.println(" Mode " + CalculateMode(arrayElememts));
    }

    private static double CalculateMean(int[] array) {
        int sum = 0;
        for (int i : array) {
            sum += i;
        }
        return (double) sum / array.length;
    }

    private static double CalculateMedian(int[] array) {
        Arrays.sort(array);
        int lengthOfArray = array.length;
        if (lengthOfArray % 2 != 0)
            return array[lengthOfArray / 2];

        return (array[(lengthOfArray - 1) / 2] + array[lengthOfArray / 2]) / 2.0;
    }

    private static double CalculateMode(int[] array) {
        int maximumValue = 0;
        int maximumFrequency = 0;
        int lengthOfArray = array.length;
        for (int i = 0; i < lengthOfArray; ++i) {
            int count = 0;
            for (int j = 0; j < lengthOfArray; ++j) {
                if (array[j] == array[i]) {
                    ++count;
                }

                if (count > maximumFrequency) {
                    maximumFrequency = count;
                    maximumValue = array[i];
                }
            }
        }
        return maximumValue;
    }
}
